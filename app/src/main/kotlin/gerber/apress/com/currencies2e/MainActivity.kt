package gerber.apress.com.currencies2e

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.AssetManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.io.InputStream
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.Collections
import java.util.Properties

//chapter 11 complete
class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    //define members that correspond to Views in our layout
    private var mCalcButton: Button? = null
    private var mConvertedTextView: TextView? = null
    private var mAmountEditText: EditText? = null
    private var mForSpinner: Spinner? = null
    private var mHomSpinner: Spinner? = null
    private var mCurrencies: Array<String>? = null

    //this will contain my developers key
    private var mKey: String? = null


    //create this interface for instrumentation testing with threads
    private var mCurrencyTaskCallback: CurrencyTaskCallback? = null


    val isOnline: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            return if (networkInfo != null && networkInfo.isConnectedOrConnecting) {
                true
            } else false
        }

    interface CurrencyTaskCallback {
        fun executionDone()
    }

    fun setCurrencyTaskCallback(currencyTaskCallback: CurrencyTaskCallback) {
        this.mCurrencyTaskCallback = currencyTaskCallback
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //unpack ArrayList from the bundle and convert to array
        val arrayList = intent.getSerializableExtra(SplashActivity.KEY_ARRAYLIST) as ArrayList<String>
        Collections.sort(arrayList)
        mCurrencies = arrayList.toTypedArray<String>()


        //assign references to our Views;
        mConvertedTextView = findViewById<View>(R.id.txt_converted) as TextView
        mAmountEditText = findViewById<View>(R.id.edt_amount) as EditText
        mCalcButton = findViewById<View>(R.id.btn_calc) as Button
        mCalcButton!!.setOnClickListener {
            if (isNumeric(mAmountEditText!!.text.toString())) {
                CurrencyConverterTask().execute(URL_BASE + mKey!!)
            } else {
                Toast.makeText(this@MainActivity, "Not a numeric value, try again.", Toast.LENGTH_LONG).show()
            }
        }
        mForSpinner = findViewById<View>(R.id.spn_for) as Spinner
        mHomSpinner = findViewById<View>(R.id.spn_hom) as Spinner
        //controller: mediates model and view
        val arrayAdapter = ArrayAdapter(

                //context
                this,
                //view: layout you see when the spinner is closed
                R.layout.spinner_closed,
                //model: the array of Strings
                mCurrencies!!
        )

        //view: layout you see when the spinner is open
        arrayAdapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item)

        //assign adapters to spinners
        mHomSpinner!!.adapter = arrayAdapter
        mForSpinner!!.adapter = arrayAdapter

        mHomSpinner!!.onItemSelectedListener = this
        mForSpinner!!.onItemSelectedListener = this

        //set to shared-preferences or pull from shared-preferences on restart
        if (savedInstanceState == null && PrefsMgr.getString(this, FOR) == null && PrefsMgr.getString(this, HOM) == null) {

            mForSpinner!!.setSelection(findPositionGivenCode("CNY", mCurrencies!!))
            mHomSpinner!!.setSelection(findPositionGivenCode("USD", mCurrencies!!))

            PrefsMgr.setString(this, FOR, "CNY")
            PrefsMgr.setString(this, HOM, "USD")


        } else {

            mForSpinner!!.setSelection(PrefsMgr.getString(this,
                    FOR)?.let { findPositionGivenCode(it, mCurrencies!!) }!!)

            PrefsMgr.getString(this,
                    HOM)?.let { findPositionGivenCode(it, mCurrencies!!) }?.let { mHomSpinner!!.setSelection(it) }
        }

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setLogo(R.mipmap.ic_launcher)

        mKey = getKey("open_key")


    }

    private fun getKey(keyName: String): String {
        val assetManager = this.resources.assets
        val properties = Properties()
        try {
            val inputStream = assetManager.open("keys.properties")
            properties.load(inputStream)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        return properties.getProperty(keyName)

    }

    private fun findPositionGivenCode(code: String, currencies: Array<String>): Int {

        for (i in currencies.indices) {
            if (extractCodeFromCurrency(currencies[i]).equals(code, ignoreCase = true)) {
                return i
            }
        }
        //default
        return 0
    }

    private fun extractCodeFromCurrency(currency: String): String {
        return currency.substring(0, 3)
    }

    private fun launchBrowser(strUri: String) {
        if (isOnline) {
            val uri = Uri.parse(strUri)
            //call an implicit intent
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    private fun invertCurrencies() {
        val nFor = mForSpinner!!.selectedItemPosition
        val nHom = mHomSpinner!!.selectedItemPosition
        mForSpinner!!.setSelection(nHom)
        mHomSpinner!!.setSelection(nFor)
        mConvertedTextView!!.text = ""

        PrefsMgr.setString(this, FOR, extractCodeFromCurrency(mForSpinner!!.selectedItem as String))
        PrefsMgr.setString(this, HOM, extractCodeFromCurrency(mHomSpinner!!.selectedItem as String))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        when (id) {
            R.id.mnu_invert -> invertCurrencies()
            R.id.mnu_codes -> launchBrowser(SplashActivity.URL_CODES)
            R.id.mnu_exit -> finish()
        }
        return true
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

        when (parent.id) {

            R.id.spn_for -> PrefsMgr.setString(this, FOR,
                    extractCodeFromCurrency(mForSpinner!!.selectedItem as String))

            R.id.spn_hom -> PrefsMgr.setString(this, HOM,
                    extractCodeFromCurrency(mHomSpinner!!.selectedItem as String))

            else -> {
            }
        }

        mConvertedTextView!!.text = ""

    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private inner class CurrencyConverterTask : AsyncTask<String, Void, JSONObject>() {
        private var progressDialog: ProgressDialog? = null
        override fun onPreExecute() {
            progressDialog = ProgressDialog(this@MainActivity)
            progressDialog!!.setTitle("Calculating Result...")
            progressDialog!!.setMessage("One moment please...")
            progressDialog!!.setCancelable(true)
            progressDialog!!.setButton(DialogInterface.BUTTON_NEGATIVE,
                    "Cancel") { dialog, which ->
                this@CurrencyConverterTask.cancel(true)
                progressDialog!!.dismiss()
            }
            progressDialog!!.show()
        }

        override fun doInBackground(vararg params: String): JSONObject? {
            try {
                return JsonParser.fetchJson(params[0])
            } catch (e: Exception) {
                return null
            }

        }

        override fun onPostExecute(jsonObject: JSONObject?) {
            var dCalculated = 0.0
            val strForCode = extractCodeFromCurrency(mCurrencies!![mForSpinner!!.selectedItemPosition])
            val strHomCode = extractCodeFromCurrency(mCurrencies!![mHomSpinner!!.selectedItemPosition])
            val strAmount = mAmountEditText!!.text.toString()
            try {
                if (jsonObject == null) {
                    throw JSONException("no data available.")
                }
                val jsonRates = jsonObject.getJSONObject(RATES)
                if (strHomCode.equals("USD", ignoreCase = true)) {
                    dCalculated = java.lang.Double.parseDouble(strAmount) / jsonRates.getDouble(strForCode)
                } else if (strForCode.equals("USD", ignoreCase = true)) {
                    dCalculated = java.lang.Double.parseDouble(strAmount) * jsonRates.getDouble(strHomCode)
                } else {
                    dCalculated = java.lang.Double.parseDouble(strAmount) * jsonRates.getDouble(strHomCode) / jsonRates.getDouble(strForCode)
                }
            } catch (e: JSONException) {
                Toast.makeText(
                        this@MainActivity,
                        "There's been a JSON exception: " + e.message,
                        Toast.LENGTH_LONG
                ).show()
                mConvertedTextView!!.text = ""
                e.printStackTrace()
            }

            mConvertedTextView!!.text = DECIMAL_FORMAT.format(dCalculated) + " " + strHomCode
            progressDialog!!.dismiss()

            //for testing
            if (mCurrencyTaskCallback != null) {
                mCurrencyTaskCallback!!.executionDone()
            }
        }
    }

    companion object {

        val FOR = "FOR_CURRENCY"
        val HOM = "HOM_CURRENCY"
        //used to fetch the 'rates' json object from openexchangerates.org
        val RATES = "rates"
        val URL_BASE = "http://openexchangerates.org/api/latest.json?app_id="
        //used to format data from openexchangerates.org
        private val DECIMAL_FORMAT = DecimalFormat("#,##0.00000")

        fun isNumeric(str: String): Boolean {
            try {
                val dub = java.lang.Double.parseDouble(str)
            } catch (nfe: NumberFormatException) {
                return false
            }

            return true
        }
    }
}

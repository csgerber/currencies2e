package gerber.apress.com.currencies2e

import org.json.JSONException
import org.json.JSONObject

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

object JsonParser {

    private val USER_AGENT = "Mozilla/5.0"
    @Throws(Exception::class)
    fun fetchJson(strUrl: String): JSONObject? {

        val url = URL(strUrl)
        val httpURLConnection = url.openConnection() as HttpURLConnection
        httpURLConnection.requestMethod = "GET"
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT)
        val responseCode = httpURLConnection.responseCode
        if (responseCode == 200) {
            val bufferedReader = BufferedReader(
                    InputStreamReader(httpURLConnection.inputStream))
            var inputLine: String
            val stringBuffer = StringBuffer()


            bufferedReader.forEachLine {
                    stringBuffer.append(it)
            }
            bufferedReader.close()
            try {
                return JSONObject(stringBuffer.toString())
            } catch (e: JSONException) {
                return null
            }

        } else {
            return null
        }

    }


}
package gerber.apress.com.currencies2e

/**
 * Created by Adam Gerber on 5/3/2015.
 */
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PrefsMgr {

    private var sSharedPreferences: SharedPreferences? = null

    fun setString(context: Context, locale: String, code: String) {
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sSharedPreferences!!.edit()
        editor.putString(locale, code)
        editor.commit()

    }

    fun getString(context: Context, locale: String): String? {
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sSharedPreferences!!.getString(locale, null)

    }


}
package gerber.apress.com.currencies2e

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList

//chapter 9 complete
class SplashActivity : AppCompatActivity() {
    //ArrayList of currencies that will be fetched and passed into MainActivity
    private var mCurrencies: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        FetchCodesTask().execute(URL_CODES)
    }

    private inner class FetchCodesTask : AsyncTask<String, Void, JSONObject>() {
        override fun doInBackground(vararg params: String): JSONObject? {
            try {
                return JsonParser.fetchJson(params[0])
            } catch (e: Exception) {
                return null
            }

        }

        override fun onPostExecute(jsonObject: JSONObject?) {
            try {
                if (jsonObject == null) {
                    throw JSONException("no data available.")
                }
                val iterator = jsonObject.keys()
                var key = ""
                mCurrencies = ArrayList()
                while (iterator.hasNext()) {
                    key = iterator.next() as String
                    mCurrencies!!.add(key + " | " + jsonObject.getString(key))
                }
                val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                mainIntent.putExtra(KEY_ARRAYLIST, mCurrencies)
                startActivity(mainIntent)
                finish()
            } catch (e: JSONException) {
                Toast.makeText(
                        this@SplashActivity,
                        "There's been a JSON exception: " + e.message,
                        Toast.LENGTH_LONG
                ).show()
                e.printStackTrace()
                finish()
            }

        }
    }

    companion object {
        //url to currency codes used in this application
        val URL_CODES = "http://openexchangerates.org/api/currencies.json"
        val KEY_ARRAYLIST = "key_arraylist"
    }
}
